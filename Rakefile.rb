require_relative 'config/constants'
require_relative './api'
require 'rake'
require 'rake/testtask'


def remove_test_dir
  FileUtils.rm_rf Dir.glob(Constants::STORE_TEST_DIR)
end

Rake::TestTask.new do |t|
  ENV['RACK_ENV'] = 'test'
  t.pattern = 'test/**/*_test.rb'
  remove_test_dir
end

task :test_and_clean => ['test'] do
  remove_test_dir
end

task default: :test_and_clean
