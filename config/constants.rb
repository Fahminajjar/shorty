module Constants

  SHORTCODE_CHARACTERS = ('a'..'z').to_a + ('A'..'Z').to_a + ('0'..'9').to_a + ['_']
  SHORTCODE_LENGTH = 6

  STORE_DIR = 'store'
  STORE_TEST_DIR = 'store/test'

end
