require_relative '../../config/constants'
require_relative '../../lib/shorty_error'
require_relative '../../lib/storage'
require_relative '../../lib/shorty'
require_relative '../../lib/helpers'


class ShortyTest < Test::Unit::TestCase
  include Rack::Test::Methods

  @@shortcode_regex = /^[0-9a-zA-Z_]{6}$/

  def assert_shortcode_match_regex code
    assert !(code =~ @@shortcode_regex).nil?
  end

  def assert_shorty_with_hash shorty, hash
    assert_equal shorty.url, hash[:url]
    assert_equal shorty.start_date, hash[:start_date]
    assert_equal shorty.last_seen_date, hash[:last_seen_date]
    assert_equal shorty.redirect_count, hash[:redirect_count]
  end


  def test_initializer_with_empty_url
    begin
      Shorty.new(url: '')
      error_raised = false
    rescue ShortyError => err
      error_raised = true
      assert_equal err.code, 400
      assert_equal err.message, 'url is not present'
    end
    assert error_raised
  end

  def test_initializer_without_url
    begin
      Shorty.new()
      error_raised = false
    rescue ShortyError => err
      error_raised = true
      assert_equal err.code, 400
      assert_equal err.message, 'url is not present'
    end
    assert error_raised
  end

  def test_initializer_with_invalid_shortcode

  end

  def test_initializer_without_providing_shortcode
    shorty = Shorty.new(url: 'https://www.impraise.com')
    assert !shorty.shortcode.empty?
    assert_shortcode_match_regex shorty.shortcode
  end

  def test_initializer_with_shortcode
    shortcode = 'abcd'
    shorty = Shorty.new(url: 'https://www.impraise.com', shortcode: shortcode)
    assert !shorty.shortcode.empty?
    assert shorty.shortcode == shortcode
  end

  def test_initializer_with_params
    params = {
      url: 'https://www.impraise.com',
      shortcode: 'abcdef',
      start_date: Helpers.time_iso8601,
      last_seen_date: Helpers.time_iso8601,
      redirect_count: 5,
    }
    shorty = Shorty.new(params)
    assert_shorty_with_hash shorty, params
  end

  def test_generate_shortcode
    shorty = Shorty.new(url: 'https://www.impraise.com')
    code = shorty.generate_shortcode
    assert_shortcode_match_regex code
  end

  def test_regenerate_shortcode
    shorty = Shorty.new(url: 'https://www.impraise.com')
    old_shortcode = shorty.shortcode
    shorty.regenerate
    assert old_shortcode != shorty.shortcode
  end

  def test_get_with_empty_str
    shorty = Shorty.get('')
    assert_equal shorty, nil
  end

  def test_get_with_non_existent_shortcode
    shorty = Shorty.get('non_existent_shortcode')
    assert_equal shorty, nil
  end

  def test_get_with_existent_shortcode
    shortcode = 'shorty_get'
    Shorty.new(url: 'https://www.impraise.com', shortcode: shortcode).create

    shorty = Shorty.get(shortcode)
    assert_equal shorty.shortcode, shortcode
  end

  def test_exists_with_non_existent_shorty
    shorty = Shorty.new(url: 'https://www.impraise.com', shortcode: 'non_existent_shorty')
    assert !shorty.exists?
  end

  def test_exists_with_existent_shorty
    shorty = Shorty.new(url: 'https://www.impraise.com', shortcode: 'existent_shorty').create
    assert shorty.exists?
  end

  def test_create_with_existent_shortcode
    shortcode = 'create__existent_shortcode'
    Shorty.new(url: 'https://www.impraise.com', shortcode: shortcode).create
    assert Storage.exists?(shortcode)

    begin
      Shorty.new(url: 'https://www.impraise.com', shortcode: shortcode).create
      error_raised = false
    rescue ShortyError => err
      error_raised = true
      assert_equal err.code, 409
      assert_equal err.message, 'The desired shortcode is already in use'
    end
    assert error_raised
  end

  def test_create_with_non_existent_shortcode
    shortcode = 'create__nonexistent_shortcode'
    assert !Storage.exists?(shortcode)

    begin
      Shorty.new(url: 'https://www.impraise.com', shortcode: shortcode).create
      exception_raised = false
    rescue ShortyError
      exception_raised = true
    end

    assert !exception_raised
    assert Storage.exists?(shortcode)
  end

  def test_update_stats
    shorty = Shorty.new(url: 'https://www.impraise.com', shortcode: 'update_stats').create
    current_last_seen_date = shorty.last_seen_date
    current_redirect_count = shorty.redirect_count

    assert shorty.update_stats
    assert shorty.last_seen_date != current_last_seen_date
    assert shorty.redirect_count == current_redirect_count + 1
  end

end
