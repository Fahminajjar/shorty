require_relative '../../config/constants'
require_relative '../../lib/storage'
require_relative '../../lib/shorty'
require_relative '../../lib/helpers'


class StorageTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def assert_shorty_with_hash shorty, hash
    assert_equal shorty.url, hash[:url]
    assert_equal shorty.start_date, hash[:start_date]
    assert_equal shorty.last_seen_date, hash[:last_seen_date]
    assert_equal shorty.redirect_count, hash[:redirect_count]
  end


  def test_get_dir_with_test_env
    ENV['RACK_ENV'] = 'test'
    dir = Storage.get_dir
    assert_equal dir, File.absolute_path(File.join(Dir.pwd, Constants::STORE_TEST_DIR))
  end

  def test_get_dir_with_production_env
    ENV['RACK_ENV'] = 'production'
    dir = Storage.get_dir
    assert_equal dir, File.absolute_path(File.join(Dir.pwd, Constants::STORE_DIR))
    ENV['RACK_ENV'] = 'test'
  end

  def test_shortcode_exists_false
    assert !Storage.exists?('testco')
  end

  def test_shortcode_exists_empty_str
    assert !Storage.exists?('')
  end


  def test_get_with_empty_shortcode
    assert_equal Storage.get(''), nil
  end

  def test_get_with_non_existent_shortcode
    assert_equal Storage.get('non_existent'), nil
  end

  def test_get_with_existent_shortcode
    shortcode = 'existent_shortcode123'
    Shorty.new(url: 'http://www.impraise.com', shortcode: shortcode).create
    assert Storage.get(shortcode)
  end

  def test_create
    shorty = Shorty.new(url: 'http://www.impraise.com', shortcode: 'dummydummy', start_date: Helpers.time_iso8601)
    Storage.create shorty
    data = Storage.get(shorty.shortcode)
    assert_shorty_with_hash(shorty, data)
  end

  def test_update
    shorty = Shorty.new(url: 'http://www.impraise.com', shortcode: 'dummmy22').create
    Storage.update shorty
    data = Storage.get(shorty.shortcode)
    assert_shorty_with_hash(shorty, data)
  end


end
