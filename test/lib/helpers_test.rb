require_relative '../../lib/helpers'


class HelpersTest < Test::Unit::TestCase
  include Rack::Test::Methods

  def test_time_iso8601
    time = Helpers.time_iso8601
    valid = true
    Time.iso8601(time) rescue valid = false
    assert valid
  end

  def test_time_iso8601_with_timestamp
    before24h = (Time.now - 3600 * 24).to_f
    time = Helpers.time_iso8601 before24h

    time_iso = Time.at(before24h).utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ')
    assert_equal time_iso, time
  end


  def test_valid_shortcode_with_empty_str
    valid = Helpers.valid_shortcode?('')
    assert !valid
  end

  def test_valid_shortcode_with_less_than_4_characters
    valid = Helpers.valid_shortcode?('a7f')
    assert !valid
  end

  def test_valid_shortcode_with_alphanumeric
    valid = Helpers.valid_shortcode?('a7Vf5')
    assert valid
  end

  def test_valid_shortcode_with_symbols
    valid = Helpers.valid_shortcode?('a6G$%')
    assert !valid
  end

  def test_valid_url_with_empty_str
    valid = Helpers.valid_url?('')
    assert !valid
  end

  def test_valid_url_with_invalid_url
    valid = Helpers.valid_url?('wwwgoogle.com')
    assert !valid
  end

  def test_valid_url_with_valid_url
    valid = Helpers.valid_url?('https://www.impraise.com')
    assert valid
  end
end
