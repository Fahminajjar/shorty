require_relative '../api'

require 'test/unit'
require 'rack/test'


class APITest < Test::Unit::TestCase
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end

  class ShortenPOSTEndpoint < APITest
    def test_shorten_without_url
      post '/shorten'
      assert_equal last_response.status, 400
      assert last_response.body.include?('url is not present')
    end

    def test_invalid_url
      post '/shorten', { url: 'wwwgooglecom' }.to_json
      assert_equal last_response.status, 400
      assert last_response.body.include?('url is invalid')
    end

    def test_shortcode_already_exists
      post '/shorten', { url: 'https://www.impraise.com' }.to_json
      assert_equal last_response.status, 201
      rsp = JSON.parse(last_response.body, symbolize_names: true)

      post '/shorten', { url: 'https://www.impraise.com', shortcode: rsp[:shortcode] }.to_json
      assert_equal last_response.status, 409
      assert last_response.body.include?('already in use')
    end

    def test_regexp_fails
      post '/shorten', { url: 'https://www.impraise.com', shortcode: '3Ftg_$' }.to_json
      assert_equal last_response.status, 422
      assert last_response.body.include?('The shortcode is invalid')
    end

    def test_shortcode_created
      post '/shorten', { url: 'https://www.impraise.com' }.to_json
      assert_equal last_response.status, 201
      rsp = JSON.parse(last_response.body, symbolize_names: true)
      assert rsp.has_key?(:shortcode)
    end
  end

  class ShortcodeGETEndpoint < APITest
    def test_shortcode_not_found
      get '/dummycode'
      assert_equal last_response.status, 404
    end

    def test_redirect
      url = 'https://www.impraise.com'
      post '/shorten', { url: url }.to_json
      assert_equal last_response.status, 201
      rsp = JSON.parse(last_response.body, symbolize_names: true)

      get "/#{rsp[:shortcode]}"
      assert_equal last_response.status, 302
      assert_equal last_response.location, url
    end
  end

  class ShortcodeStatsGETEndpoint < APITest
    def test_shortcode_not_found
      get '/dummycode/stats'
      assert_equal last_response.status, 404
    end

    def test_stats
      url = 'https://www.impraise.com'
      post '/shorten', { url: url }.to_json
      assert_equal last_response.status, 201
      rsp = JSON.parse(last_response.body, symbolize_names: true)

      shortcode = rsp[:shortcode]
      get "/#{shortcode}/stats"
      assert_equal last_response.status, 200
      rsp = JSON.parse(last_response.body, symbolize_names: true)
      [:startDate, :redirectCount].each{ |key| assert rsp.has_key?(key) }
      assert !rsp.has_key?(:lastSeenDate)
      assert_equal rsp[:redirectCount], 0

      get "/#{shortcode}"

      get "/#{shortcode}/stats"
      assert_equal last_response.status, 200
      rsp = JSON.parse(last_response.body, symbolize_names: true)
      assert rsp.has_key?(:lastSeenDate)
      assert_equal rsp[:redirectCount], 1
    end
  end

end
