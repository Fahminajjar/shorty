require 'json'


module Storage
  module_function

  def get_dir
    if ENV['RACK_ENV'] == 'test'
      Dir.mkdir(Constants::STORE_TEST_DIR) if Dir[Constants::STORE_TEST_DIR].size == 0
      dir = Constants::STORE_TEST_DIR
    else
      dir = Constants::STORE_DIR
    end
    File.absolute_path(File.join(Dir.pwd, dir))
  end

  STORE_DIR = self.get_dir

  def exists? shortcode
    Dir["#{STORE_DIR}/#{shortcode}.json"].size == 1
  end

  def get shortcode
    begin
      file = File.read("#{STORE_DIR}/#{shortcode}.json")
      JSON.parse(file, symbolize_names: true)
    rescue
      return nil
    end
  end

  def write shorty, mode
    res = File.write("#{STORE_DIR}/#{shorty.shortcode}.json", {
        url: shorty.url,
        start_date: shorty.start_date,
        redirect_count: shorty.redirect_count,
        last_seen_date: shorty.last_seen_date
      }.to_json, mode: mode)
    res > 0
  end

  def create shorty
    self.write(shorty, 'w')
  end

  def update shorty
    self.write(shorty, 'r+')
  end

end
