class Shorty

  attr_reader :url, :shortcode, :start_date, :last_seen_date, :redirect_count

  def generate_shortcode
    Constants::SHORTCODE_CHARACTERS.shuffle.sample(Constants::SHORTCODE_LENGTH).join
  end

  def self.get(shortcode)
    return nil if !shortcode || !Storage.exists?(shortcode)

    data = Storage.get(shortcode)
    data[:shortcode] = shortcode
    Shorty.new(data)
  end

  def initialize(url: nil, shortcode: nil, start_date: nil, last_seen_date: nil, redirect_count: 0)
    raise ShortyError.new('url is not present', 400) if !url || url.empty?
    raise ShortyError.new('url is invalid', 400) if !Helpers.valid_url?(url)

    if shortcode
      if !Helpers.valid_shortcode?(shortcode)
        raise ShortyError.new('The shortcode is invalid, it must be alphanumeric', 422)
      end
    end

    @url = url
    @start_date = start_date
    @last_seen_date = last_seen_date
    @redirect_count = redirect_count

    if !shortcode
      @shortcode = self.generate_shortcode
      until !self.exists?
        self.regenerate
      end
    else
      @shortcode = shortcode
    end
  end

  def exists?
    Storage.exists?(@shortcode)
  end

  def regenerate
    @shortcode = self.generate_shortcode
  end

  def create
    raise ShortyError.new('The desired shortcode is already in use', 409) if self.exists?

    @start_date = Helpers.time_iso8601
    return Storage.create(self) ? self : nil
  end

  def update_stats
    @last_seen_date = Helpers.time_iso8601
    @redirect_count += 1
    Storage.update(self)
  end

end
