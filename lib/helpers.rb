require 'uri'

module Helpers
  module_function

  def time_iso8601 timestamp=nil
    timestamp = Time.now.to_f if !timestamp
    Time.at(timestamp).utc.strftime('%Y-%m-%dT%H:%M:%S.%LZ')
  end

  def valid_shortcode?(shortcode)
    !(shortcode =~ /^[0-9a-zA-Z_]{4,}$/).nil?
  end

  def valid_url?(url)
    !(url =~ /^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/ix).nil?
  end

end
