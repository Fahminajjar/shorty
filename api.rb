require 'sinatra'
require 'multi_json'


set :bind, '0.0.0.0'
set :port, 5000


helpers do
  def json(json)
    MultiJson.dump(json, pretty: true)
  end

  def parsed_request_body
    return params if request.body.size == 0

    if request.content_type.include?('multipart/form-data;')
      parsed = params
    else
      parsed = MultiJson.load(request.body, symbolize_keys: true)
    end
    halt(400, json({ message: 'The request body you provide must be a JSON hash' })) unless parsed.is_a?(Hash)
    parsed
  end
end


before do
  content_type :json
  if request.request_method != 'GET'
    @params = parsed_request_body
  end
end


post '/shorten' do
  url = params[:url]
  shortcode = params[:shortcode]

  begin
    shorty = Shorty.new(url: url, shortcode: shortcode)
    shorty.create

    status 201
    json({ shortcode: shorty.shortcode })
  rescue ShortyError => err
    halt err.code, json({ message: err.message })
  end
end


get '/:shortcode' do
  shortcode = params[:shortcode]

  shorty = Shorty.get(shortcode)
  halt 404, json({ message: 'The shortcode cannot be found in the system' }) if !shorty

  shorty.update_stats
  redirect shorty.url
end


get '/:shortcode/stats' do
  shortcode = params[:shortcode]

  shorty = Shorty.get(shortcode)
  halt 404, json({ message: 'The shortcode cannot be found in the system' }) if !shorty

  rsp = {
    startDate: shorty.start_date,
    redirectCount: shorty.redirect_count
  }
  rsp[:lastSeenDate] = shorty.last_seen_date if shorty.redirect_count > 0
  json(rsp)
end
