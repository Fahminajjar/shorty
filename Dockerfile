FROM ruby:2.5.1

ADD . /app
WORKDIR /app

RUN bundle install
# run tests
RUN rake

ENV RACK_ENV='production'
EXPOSE 5000
