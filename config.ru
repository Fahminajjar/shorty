require './api'

Dir["#{File.dirname(__FILE__)}/config/*.rb"].each {|file| require file }
Dir["#{File.dirname(__FILE__)}/lib/*.rb"].each {|file| require file }

run Sinatra::Application
